<?php

class Kategori extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'Kategori';
  }

  public function index(){
    echo 'Kategori';
  }

  public function getData()
  {
    $data = Modules::run("database/get", array(
      'table'=> 'kategori_produk',
    ));

    $result = array();
    if(!empty($data)){
      foreach ($data->result_array() as $value) {
        $value['file'] = str_replace(" ", "_", $value['file']);
        $value['file'] = base_url().'files/berkas/kategori/'.$value['file'];
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data'=> $result
    ));
  }
  
  public function getDetailArea($area)
  {
    $data = Modules::run("database/get", array(
      'table'=> 'area_service',
      'where' => array('area'=> $area)
    ));

    $result = array();
    if(!empty($data)){
      foreach ($data->result_array() as $value) {
        array_push($result, $value['wilayah']);
      }
    }


    $str_wilayah = implode("\n", $result);
    return $str_wilayah;
  }
}
