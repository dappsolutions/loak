<?php

class Akun extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'akun';
  }

  public function index()
  {
    echo 'akun';
  }

  public function detail()
  {
    $user_id = $this->input->post('user_id');
    // $user_id = 4;
    $hak_akses = $this->input->post('hak_akses');
    $data = Modules::run('database/get', array(
      'table' => "user u",
      'field' => array('u.*'),
      'where' => array('u.id' => $user_id)
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['nama'] = $value['nama'] == "" ? '-' : $value['nama'];
        $value['no_hp'] = $value['no_hp'] == "" ? '-' : $value['no_hp'];
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  public function ubah(){
    $user_id = $this->input->post('user_id');
    $nama = $this->input->post('nama');
    $username = $this->input->post('username');
    $no_hp = $this->input->post('no_hp');

    $is_valid = false;
    $this->db->trans_begin();
    try {
      $post_user['nama'] = $nama;
      $post_user['username'] = $username;
      $post_user['no_hp'] = $no_hp;

      Modules::run("database/_update", "user", $post_user, array('id'=> $user_id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (\Throwable $th) {
      $is_valid = false;
      $this->db->trans_rollback();
    }

    echo json_encode(array(
      'is_valid'=> $is_valid
    ));
  }
}
