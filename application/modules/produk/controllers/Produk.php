<?php

class Produk extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'produk';
  }

  public function index()
  {
    echo 'produk';
  }

  public function getData()
  {
    error_reporting(0);
    $kategori = $this->input->post('kategori');
    // $kategori = 1;
    $data = Modules::run("database/get", array(
      'table' => 'produk p',
      'field' => array('p.*', 'ph.harga', 'ph.harga_antar', 'kp.kategori'),
      'join' => array(
        array('kategori_produk kp', 'p.kategori_produk = kp.id'),
        array('produk_harga ph', 'p.id = ph.produk and ph.period_end is null')
      ),
      'where' => array(
        'p.kategori_produk' => $kategori,
        'p.deleted'=> false
      )
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        if(trim($value['file']) != ""){
          $value['file'] = str_replace(" ", "_", $value['file']);
          $is_thumbnail = $this->getFileThumbnail($value['file']);
          if($is_thumbnail){            
            $value['file'] = base_url() . 'files/berkas/produk/thumb_' . $value['file'];
          }
        }
        $value['harga'] = number_format($value['harga'], 2);
        $value['harga_antar'] = number_format($value['harga_antar'], 2);
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  public function simpan()
  {
    $file_name = $this->input->post('file_name');
    $base64_file = $this->input->post('base64_file');
    $nama_produk = $this->input->post('nama_produk');
    $kategori_produk = $this->input->post('kategori_produk');
    $id = $this->input->post('id');
    $harga_antar = $this->input->post('harga_antar');
    $harga_outlet = $this->input->post('harga_outlet');
    $nama_file_exist = $this->input->post('nama_file_exist');

    if ($id != "") {
      if ($nama_file_exist != $file_name) {
        $is_upload = $this->uploadFile($file_name, $base64_file, "produk");
      } else {
        $is_upload = true;
      }
    } else {
      $is_upload = $this->uploadFile($file_name, $base64_file, "produk");
    }

    $is_valid = false;
    if ($is_upload) {
      $data['nama_produk'] = $nama_produk;
      $data['kategori_produk'] = $kategori_produk;
      $data['file'] = $file_name;

      if ($id == "") {
        $produk = Modules::run('database/_insert', "produk", $data);
        $data_harga['produk'] = $produk;
        $data_harga['harga_antar'] = $harga_antar;
        $data_harga['harga'] = $harga_outlet;
        $data_harga['period_start'] = date('Y-m-d');

        Modules::run('database/_insert', "produk_harga", $data_harga);
      } else {
        Modules::run('database/_update', 'produk', $data, array('id' => $id));

        Modules::run('database/_update', "produk_harga", array('period_end'=> date('Y-m-d')), array('produk'=> $id));
        $data_harga['produk'] = $id;
        $data_harga['harga_antar'] = $harga_antar;
        $data_harga['harga'] = $harga_outlet;
        $data_harga['period_start'] = date('Y-m-d');                
        Modules::run('database/_insert', "produk_harga", $data_harga);
      }
      $is_valid = true;
    }

    echo json_encode(array(
      'is_valid' => $is_valid
    ));
  }

  function getFileThumbnail($file){
    $base = $_SERVER['DOCUMENT_ROOT'].'/loak/';
    $src    = base_url().'files/berkas/produk/'.$file;
    $max_w  = 400;// Output maximum width
    $max_h  = 400;// Output maximum height
    $dir    = $base.'files/berkas/produk'; // Output directory
    $fle    = "thumb_".$file;// Output filename
    return $this->createThumbnail($src, $max_w, $max_h, $dir, $fle);
  }

  function createThumbnail( $src, $max_w, $max_h, $dir, $fle ) {
    $img_url = file_get_contents( $src );
    $img = imagecreatefromstring( $img_url );
    
    $old_x = imagesx( $img );   // Source image width
    $old_y = imagesy( $img );   // Source image height

    
    // Conditions for maintaining output aspect ratio
    switch ( true ) {
        case ( $old_x > $old_y ):   // If source image is in landscape orientation
            $thumb_w = $max_w;
            $thumb_h = $old_y / $old_x * $max_w;
            break;
        case ( $old_x < $old_y ):   // If source image is in portrait orientation
            $thumb_w  = $old_x / $old_y * $max_h;
            $thumb_h  = $max_h;
            break;
        case ( $old_x == $old_y ):  // If source image is a square
            $thumb_w = $max_w;
            $thumb_h = $max_h;
            break;
    }

    $thumb = imagecreatetruecolor( $thumb_w, $thumb_h );

    imagecopyresampled( $thumb, $img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y );

    $ext1 = explode(".",trim($src));
    $ext = array_slice($ext1,-1);

    $ext = trim($ext[0]);

    $result = 0;
    switch($ext):
      case 'jpg' or 'jpeg':
          $result = imagejpeg( $thumb, $dir.'/' . $fle , 100);
      break;
      case 'gif':
          $result = imagegif( $thumb, $dir.'/' . $fle , 100);
      break;
      case 'png':
          $result = imagepng( $thumb, $dir.'/' . $fle , 100);
      break;
  endswitch;

    imagedestroy( $thumb ); 
    imagedestroy( $img );

    return $result;
}

  public function uploadFile($file_name, $base64_file, $path)
  {
    $is_valid = false;
    try {
      $imageData = $base64_file;
      $base64 = $imageData;
      if (trim($base64) != "") {
        $filename = $file_name;
        // $extension = "png";

        // $filename = 'files/berkas/'.$path.'/' . $filename . "." . $extension; // path and name of image, change as per your need          
        $filename = 'files/berkas/' . $path . '/' . $filename; // path and name of image, change as per your need          
        //
        $base64 = str_replace(" ", "+", $base64);
        if ($base64 == '') {
          $is_valid = false;
          return $is_valid;
        }
        //
        $binarydata = base64_decode($base64);
        //
        //     echo $filename.'<br/>';
        $result = file_put_contents($filename, $binarydata);
        if ($result) {
          @chmod($filename, 0777);
          $is_valid = true;
        }
      }
    } catch (Exception $e) {

      $is_valid = false;
    }

    return $is_valid;
  }
}
