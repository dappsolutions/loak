<?php

class Area extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getModuleName()
	{
		return 'area';
	}

	public function index()
	{
		echo 'area';
	}

	public function getDataArea()
	{
		$data = Modules::run("database/get", array(
			'table' => 'area',
			'where'=> array('deleted'=> 0)
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['file'] = $value['gambar'];
				$value['gambar'] = str_replace(" ", "%20", $value['gambar']);
				$value['gambar'] = base_url() . 'files/berkas/area/' . $value['gambar'];				
				$value['detail_area'] = $this->getDetailArea($value['id']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function getDataAreaServis()
	{
		$area = $this->input->post('id');
		// $area = 1;
		$data = Modules::run("database/get", array(
			'table' => 'area_service asr',
			'field' => array('asr.*'),
			'where' => array('asr.area' => $area, 'asr.deleted' => 0)
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function getDetailArea($area)
	{
		$data = Modules::run("database/get", array(
			'table' => 'area_service',
			'where' => array('area' => $area)
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value['wilayah']);
			}
		}


		$str_wilayah = implode("\n", $result);
		return $str_wilayah;
	}

	public function simpan()
	{
		// $data['area'] = 'Surabaya';
		//   $data['gambar'] = 's.jpg';
		// 		Modules::run('database/_insert', 'area', $data);die;
		$file_name = $this->input->post('file_name');
		$base64_file = $this->input->post('base64_file');
		$path = $this->input->post('path');
		$nama_area = $this->input->post('nama_area');
		$id = $this->input->post('id');
		$nama_file_exist = $this->input->post('nama_file_exist');

		if ($id != "") {
			if ($nama_file_exist != $file_name) {
				$is_upload = $this->uploadFile($file_name, $base64_file, $path);
			} else {
				$is_upload = true;
			}
		} else {
			$is_upload = $this->uploadFile($file_name, $base64_file, $path);
		}

		$is_valid = false;
		if ($is_upload) {
			$data['area'] = $nama_area;
			$data['gambar'] = $file_name;

			if ($id == "") {
				Modules::run('database/_insert', 'area', $data);
				// echo '<pre>';
				// print_r($data);die;
				// echo $this->db->last_query();die;
			} else {
				Modules::run('database/_update', $path, $data, array('id' => $id));
			}
			$is_valid = true;
		}

		echo json_encode(array(
			'is_valid' => $is_valid
		));
	}

	public function simpanServis()
	{
		$wilayah = $this->input->post('wilayah');
		$id = $this->input->post('id');
		$area = $this->input->post('area');

		$is_valid = false;
		$data['wilayah'] = $wilayah;
		$data['area'] = $area;

		// echo '<pre>';
		// print_r($data);die;
		if ($id == "") {
			Modules::run('database/_insert', 'area_service', $data);
		} else {
			Modules::run('database/_update', 'area_service', $data, array('id' => $id));
		}
		$is_valid = true;

		echo json_encode(array(
			'is_valid' => $is_valid
		));
	}

	public function uploadFile($file_name, $base64_file, $path)
	{
		$is_valid = false;
		try {
			$imageData = $base64_file;
			$base64 = $imageData;
			if (trim($base64) != "") {
				$filename = $file_name;
				// $extension = "png";

				// $filename = 'files/berkas/'.$path.'/' . $filename . "." . $extension; // path and name of image, change as per your need          
				$filename = 'files/berkas/' . $path . '/' . $filename; // path and name of image, change as per your need          
				//
				$base64 = str_replace(" ", "+", $base64);
				if ($base64 == '') {
					$is_valid = false;
					return $is_valid;
				}
				//
				$binarydata = base64_decode($base64);
				//
				//     echo $filename.'<br/>';
				$result = file_put_contents($filename, $binarydata);
				if ($result) {
					@chmod($filename, 0777);
					$is_valid = true;
				}
			}
		} catch (Exception $e) {

			$is_valid = false;
		}

		return $is_valid;
	}
}
