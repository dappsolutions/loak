<?php

class Info extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'info';
  }

  public function index(){
    echo 'info';
  }

  public function detailInfo()
  {
    $data = Modules::run("database/get", array(
      'table'=> 'info_admin',
    ))->row_array();

    $no_wa = str_replace("+", "", $data['no_wa']);
    $no_wa = trim($no_wa);
    echo $no_wa;
  }
}
