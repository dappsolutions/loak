<?php

class Agen_outlet extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'Agen_outlet';
  }

  public function index()
  {
    echo 'Agen_outlet';
  }

  public function getData()
  {
    error_reporting(0);
    $keyword = $this->input->post('keyword');

    $result = array();

    $like = array(
      array('agen', $keyword)
    );
    $data = Modules::run("database/get", array(
      'table' => 'agen',
      'like' => $like,
      'deleted'=> array('deleted'=> 0)
    ));

    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['nama'] = $value['agen'];
        if(trim($value['file']) != ""){
          $is_thumbnail = $this->getFileThumbnail($value['file'], "agen");
          if($is_thumbnail){           
            $value['file'] = str_replace(" ", "%20", $value['file']); 
            $value['file'] = base_url() . 'files/berkas/agen/thumb_' . $value['file'];
          }
        }
        $value['jenis'] = "agen";
        array_push($result, $value);
      }
    }

    //outlet
    $like = array(
      array('outlet', $keyword)
    );
    $data = Modules::run("database/get", array(
      'table' => 'outlet',
      'like' => $like,
      'deleted'=> array('deleted'=> 0)
    ));

    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['nama'] = $value['outlet'];
        if(trim($value['file']) != ""){
          $is_thumbnail = $this->getFileThumbnail($value['file'], "outlet");
          if($is_thumbnail){            
            $value['file'] = str_replace(" ", "%20", $value['file']);
            $value['file'] = base_url() . 'files/berkas/outlet/thumb_' . $value['file'];
          }
        }
        $value['jenis'] = "outlet";
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  function getFileThumbnail($file, $dir_src){
    $base = $_SERVER['DOCUMENT_ROOT'].'/loak/';
    $src    = base_url().'files/berkas/'.$dir_src.'/'.$file;
    $max_w  = 400;// Output maximum width
    $max_h  = 400;// Output maximum height
    $dir    = $base.'files/berkas/'.$dir_src; // Output directory
    $fle    = "thumb_".$file;// Output filename
    return $this->createThumbnail($src, $max_w, $max_h, $dir, $fle);
  }

  function createThumbnail( $src, $max_w, $max_h, $dir, $fle ) {
    $img_url = file_get_contents( $src );
    $img = imagecreatefromstring( $img_url );
    
    $old_x = imagesx( $img );   // Source image width
    $old_y = imagesy( $img );   // Source image height

    
    // Conditions for maintaining output aspect ratio
    switch ( true ) {
        case ( $old_x > $old_y ):   // If source image is in landscape orientation
            $thumb_w = $max_w;
            $thumb_h = $old_y / $old_x * $max_w;
            break;
        case ( $old_x < $old_y ):   // If source image is in portrait orientation
            $thumb_w  = $old_x / $old_y * $max_h;
            $thumb_h  = $max_h;
            break;
        case ( $old_x == $old_y ):  // If source image is a square
            $thumb_w = $max_w;
            $thumb_h = $max_h;
            break;
    }

    $thumb = imagecreatetruecolor( $thumb_w, $thumb_h );

    imagecopyresampled( $thumb, $img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y );

    $ext1 = explode(".",trim($src));
    $ext = array_slice($ext1,-1);

    $ext = trim($ext[0]);

    $result = 0;
    switch($ext):
      case 'jpg' or 'jpeg':
          $result = imagejpeg( $thumb, $dir.'/' . $fle , 100);
      break;
      case 'gif':
          $result = imagegif( $thumb, $dir.'/' . $fle , 100);
      break;
      case 'png':
          $result = imagepng( $thumb, $dir.'/' . $fle , 100);
      break;
  endswitch;

    imagedestroy( $thumb ); 
    imagedestroy( $img );

    return $result;
}

  public function simpan()
  {
    $file_name = $this->input->post('file_name');
    $base64_file = $this->input->post('base64_file');
    $path = $this->input->post('path');
    $no_hp = $this->input->post('no_hp');
    $alamat = $this->input->post('alamat');
    $id = $this->input->post('id');
    $nama_file_exist = $this->input->post('nama_file_exist');

    if ($id != "") {
      if ($nama_file_exist != $file_name) {
        $is_upload = $this->uploadFile($file_name, $base64_file, $path);
      } else {
        $is_upload = true;
      }
    } else {
      $is_upload = $this->uploadFile($file_name, $base64_file, $path);
    }

    $is_valid = false;
    if ($is_upload) {
      $data['no_hp'] = $no_hp;
      $data[$path] = $alamat;
      $data['file'] = $file_name;

      if ($id == "") {
        Modules::run('database/_insert', $path, $data);
      } else {
        Modules::run('database/_update', $path, $data, array('id' => $id));
      }
      $is_valid = true;
    }

    echo json_encode(array(
      'is_valid' => $is_valid
    ));
  }

  public function uploadFile($file_name, $base64_file, $path)
  {
    $is_valid = false;
    try {
      $imageData = $base64_file;
      $base64 = $imageData;
      if (trim($base64) != "") {
        $filename = $file_name;
        // $extension = "png";

        // $filename = 'files/berkas/'.$path.'/' . $filename . "." . $extension; // path and name of image, change as per your need          
        $filename = 'files/berkas/' . $path . '/' . $filename; // path and name of image, change as per your need          
        //
        $base64 = str_replace(" ", "+", $base64);
        if ($base64 == '') {
          $is_valid = false;
          return $is_valid;
        }
        //
        $binarydata = base64_decode($base64);
        //
        //     echo $filename.'<br/>';
        $result = file_put_contents($filename, $binarydata);
        if ($result) {
          @chmod($filename, 0777);
          $is_valid = true;
        }
      }
    } catch (Exception $e) {

      $is_valid = false;
    }

    return $is_valid;
  }

  public function getListData()
  {
    $table = $this->input->post('table');

    $data = Modules::run("database/get", array(
      'table' => $table,
      'where' => array('deleted' => 0)
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['alamat'] = $value[$table];
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  public function hapus(){
    $id = $this->input->post('id');
    $table = $this->input->post('table');

    Modules::run('database/_update', $table, array('deleted'=> true), array('id' => $id));
  }

  public function getKeterangan(){
    $table = $this->input->post('table');
    // $table = "ket_agen";
    $data = Modules::run("database/get", array(
      'table' => $table,
      'where' => array('deleted' => 0)
    ));

    $keterangan = "";
    if (!empty($data)) {
      $keterangan = $data->row_array()['keterangan'];
    }

    echo $keterangan;
  }
}
