<?php

class Daftar extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'daftar';
  }

  public function index(){
    echo 'daftar';
  }

  public function proses()
  {
    $nama = $this->input->post('nama');
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    $is_valid = false;
    $user_id = "";
    $hak_akses = "customer";
    try {
      $is_valid = true;
      $post_user['nama'] = $nama;
      $post_user['username'] = $username;
      $post_user['password'] = $password;
      $post_user['login_by'] = 'reguler';
      $user_id = Modules::run('database/_insert', 'user', $post_user);
    } catch (Exception $exc) {
      $is_valid = false;
    }

    echo json_encode(array(
      'is_valid' => $is_valid,
      'username' => $username,
      'user_id' => $user_id,
      'hak_akses' => $hak_akses
    ));
  }
}
