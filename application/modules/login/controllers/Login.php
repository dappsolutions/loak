<?php

class Login extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'login';
  }

  public function index()
  {
    echo 'login';
  }

  public function getDataUserDb($username, $password)
  {
    //  echo $username;die;
    $data = Modules::run('database/get', array(
      'table' => 'user u',
      'field' => array('u.*'),
      'where' => array(
        'u.username' => $username,
        'u.password' => $password,
        'u.deleted' => 0
      )
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }

    return $result;
  }
  
  public function getDataUserAdminDb($username, $password)
  {
    //  echo $username;die;
    $data = Modules::run('database/get', array(
      'table' => 'user_admin u',
      'field' => array('u.*'),
      'where' => array(
        'u.username' => $username,
        'u.password' => $password,
        'u.deleted' => 0
      )
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }

    return $result;
  }

  public function getDataUser($username, $password)
  {
    $data = $this->getDataUserDb($username, $password);
    $result = array();
    if (!empty($data)) {
      $result = $data;
    }

    return $result;
  }
  
  public function getDataUserAdmin($username, $password)
  {
    $data = $this->getDataUserAdminDb($username, $password);
    $result = array();
    if (!empty($data)) {
      $result = $data;
    }

    return $result;
  }

  public function sign_in()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    $is_valid = false;
    $hak_akses = "customer";
    $user_id = "";
    try {
      $data = $this->getDataUser($username, $password);      
      if (!empty($data)) {
        $user_id = $data['id'];
        $is_valid = true;
      }else{
        $data_admin = $this->getDataUserAdmin($username, $password);
        if(!empty($data_admin)){
          $user_id = $data_admin['id'];
          $hak_akses = "admin";
          $is_valid = true;
        }
      }
    } catch (Exception $exc) {
      $is_valid = false;
    }

    echo json_encode(array(
      'is_valid' => $is_valid,
      'username' => $username,
      'user_id' => $user_id,
      'hak_akses' => $hak_akses
    ));
  }

  public function checkUser($login_by = ""){
    $keyword = $this->input->post('keyword');
    // $keyword = "dodikitn@gmail.com";
    $data = Modules::run("database/get", array(
      'table' => 'user u',
      'where' => "u.username = '".$keyword."' and u.login_by = '".$login_by."'"
    ));

    $is_exist = false;
    $message = "User Tidak Ditemukan";
    $user_id = "";
    $hak_akses = "customer";

    if(!empty($data)){
      $is_exist = true;
      $message = "";
      $data = $data->row_array();

      $user_id = $data['id'];
    }else{
      $is_exist = true;
      $post_user['username'] = $keyword;
      $post_user['login_by'] = $login_by;
      $user_id = Modules::run('database/_insert', "user", $post_user);
      $message = "Tambah Baru";
    }

    echo json_encode(array(
      'is_exist'=> $is_exist,
      'user_id' => $user_id,
      'message'=> $message,
      'hak_akses'=> $hak_akses
    ));
  }
}
