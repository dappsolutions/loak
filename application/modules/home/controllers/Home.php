<?php

class Home extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'home';
  }

  public function index(){
    echo 'home';
  }

  public function getData()
  {
    $data = Modules::run("database/get", array(
      'table'=> 'total_sampah',
    ))->row_array();
    $total_sampah = $data['total'];
    
    $data = Modules::run("database/get", array(
      'table'=> 'total_user',
    ))->row_array();
    $total_user = $data['total'];
    
    $data = Modules::run("database/get", array(
      'table'=> 'total_agen',
    ))->row_array();
    $total_agen = $data['total'];
    
    $data = Modules::run("database/get", array(
      'table'=> 'total_daerah',
    ))->row_array();
    $total_daerah = $data['total'];
    
    $data = Modules::run("database/get", array(
      'table'=> 'total_outlet',
    ))->row_array();
    $total_outlet = $data['total'];
    
    $data = Modules::run("database/get", array(
      'table'=> 'total_mitra',
    ))->row_array();
    $total_mitra = $data['total'];


    echo json_encode(array(
      'total_sampah'=> $total_sampah,
      'total_user'=> $total_user,
      'total_agen'=> $total_agen,
      'total_daerah'=> $total_daerah,
      'total_outlet'=> $total_outlet,
      'total_mitra'=> $total_mitra,
    ));
  }
  
  public function simpanTotal()
  {
    $jenis = $this->input->post('jenis');
    $total = $this->input->post('total');
    $table = "";
    switch ($jenis) {
      case '1':
        $table = "total_sampah";
        break;
      case '2':
        $table = "total_user";
        break;
      case '3':
        $table = "total_agen";
        break;
      case '4':
        $table = "total_daerah";
        break;
      case '5':
        $table = "total_outlet";
        break;
      case '6':
        $table = "total_mitra";
        break;
      
      default:
        # code...
        break;
    }

    Modules::run('database/_update', $table, array('total'=> $total), array('id'=> 1));
  }
}
