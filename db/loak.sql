-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 05, 2019 at 05:11 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loak`
--

-- --------------------------------------------------------

--
-- Table structure for table `agen`
--

CREATE TABLE `agen` (
  `id` int(11) NOT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `agen` varchar(255) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `file` text,
  `latitude` varchar(150) NOT NULL DEFAULT '0.0',
  `longitude` varchar(150) NOT NULL DEFAULT '0.0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agen`
--

INSERT INTO `agen` (`id`, `no_hp`, `agen`, `nama`, `file`, `latitude`, `longitude`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '6285748233712', 'Jl. Blimbing Wonotirto', NULL, 'malang.jpg', '0', '0', NULL, NULL, '2019-06-04 03:58:16', NULL, 0),
(4, '6285748233712', 'jl. blitar', NULL, 'Screenshot_20190604-023206_Instagram.jpg', '0.0', '0.0', '2019-06-04', NULL, '2019-06-04 04:12:05', NULL, 1),
(5, '888', 'yuy', NULL, 'IMG-20190622-WA0006.jpg', '0.0', '0.0', '2019-06-22', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `area` varchar(45) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `area`, `gambar`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Malang', 'malang.jpg', NULL, NULL, NULL, 0, 0),
(5, 'Surabaya', 'Screenshot_20190718-095145_WhatsApp.jpg', '2019-07-20', NULL, '2019-07-21 09:00:36', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `area_service`
--

CREATE TABLE `area_service` (
  `id` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `wilayah` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_service`
--

INSERT INTO `area_service` (`id`, `area`, `wilayah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Kec. Blimbing', NULL, NULL, NULL, NULL, 0),
(2, 1, 'Kec. Singosari', NULL, NULL, NULL, NULL, 0),
(5, 5, 'Kec Wiyung', '2019-07-20', NULL, NULL, NULL, 0),
(6, 5, 'Kec Wonocolo', '2019-07-20', NULL, '2019-07-21 08:37:53', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `info_admin`
--

CREATE TABLE `info_admin` (
  `id` int(11) NOT NULL,
  `no_wa` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_admin`
--

INSERT INTO `info_admin` (`id`, `no_wa`) VALUES
(1, '6281233933300');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `file` text,
  `nama` varchar(150) DEFAULT NULL,
  `harga` int(11) DEFAULT '0',
  `no_hp` varchar(16) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `file`, `nama`, `harga`, `no_hp`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'plan.png', 'Produk Contoh Satu', 3000, '6285748233712', NULL, NULL, NULL, NULL, 0),
(2, 'IMG-20190701-WA0054.jpg', NULL, 5000, '6285748233712', '2019-07-01', NULL, '2019-07-01 16:57:13', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id` int(11) NOT NULL,
  `kategori` varchar(150) DEFAULT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`id`, `kategori`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Plastik / Botol', 'plastic-bottle.png', NULL, NULL, NULL, NULL, 0),
(2, 'Aneka Kertas', 'plan.png', NULL, NULL, NULL, NULL, 0),
(3, 'Besi / Tembaga', 'wire.png', NULL, NULL, NULL, NULL, 0),
(4, 'Elektronik', 'responsive.png', NULL, NULL, NULL, NULL, 0),
(5, 'Pakaian Donasi', 'shirt.png', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_agen`
--

CREATE TABLE `ket_agen` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_agen`
--

INSERT INTO `ket_agen` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Jika ingin bergabung menjadi agen sebuah bisnis tertentu, pilihlah bisnis yang marketable, sudah pasti dibutuhkan oleh Masyarakat, serta mampu bertahan jangka panjang. \"Bukan bisnis musiman atauhanya trend sesaat\".\r\n\r\nSadarkah Kami jika Masyarakat membeli ayam kentaki, kopi, makanan di restoran/warung, baju, dll di tempat/toko sama paling cepat hanya 1x/minggu. Tetapi Mereka menghasilkan sampah (bernilai ekonomis) hampir setiap hari, karena setiap Mereka berbelanja pasti akan membawa bungkus/pack yang terbuang sia-sia sebagai sampah karena tidak tahu harus menjualnya ke mana.\r\n', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_area`
--

CREATE TABLE `ket_area` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_area`
--

INSERT INTO `ket_area` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Agen/ Outlet Center Silahkan Isi Data Di Bawah ini:\r\nNama:\r\nAlamat Rumah:\r\nRencana Alamat Agen/ Outlet (Jika sesuai dengan alamat rumah tidak perlu diisi):\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nSaya ingin bergabung menjadi Agen/ outlet (Pilih Salah Satu):\r\nApakah Alasan Anda ingin Buka Agen/ Outlet:\r\nApakah Anda Siap Menyiapkan Modal Investasi Bergabung? (Iya/ Tidak):\r\n---------------------------------\r\nData yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Sampai jumpa di Tim Sukses Loakin Go ;)', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_donasi_pohon`
--

CREATE TABLE `ket_donasi_pohon` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_donasi_pohon`
--

INSERT INTO `ket_donasi_pohon` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Donasi Pohon Silahkan Isi Data Di Bawah ini:\r\nNama Donatur:\r\nAlamat Rumah:\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nJumlah Pohon yang Ingin Didonasikan: \r\n*1 Pohon Donasinya sebesar Rp 10.000\r\n---------------------------------\r\nData Donatur yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Terimakasih sudah ikut andil menjadi Donatur Bumi. Salam Bantu Bumi', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_donasi_sampah`
--

CREATE TABLE `ket_donasi_sampah` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_donasi_sampah`
--

INSERT INTO `ket_donasi_sampah` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Donasi Sampah Silahkan Isi Data Di Bawah ini:\r\nNama Donatur:\r\nAlamat Pengambilan:\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nJenis sampah yang ingin di donasikan (kertas/ plastik/ besi/ dll):\r\n---------------------------------\r\nData Donatur yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Terimakasih sudah ikut andil menjadi Donatur Bumi. Salam Bantu Bumi\r\n', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_harga`
--

CREATE TABLE `ket_harga` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_harga`
--

INSERT INTO `ket_harga` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Agen Silahkan Isi Data Di Bawah ini:\r\nNama:\r\nAlamat Rumah:\r\nRencana Alamat Agen (Jika sesuai dengan alamat rumah tidak perlu diisi):\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nApakah Alasan Anda ingin Buka Agen:\r\nApakah Anda Siap Menyiapkan Modal Investasi Bergabung? (Iya/ Tidak):\r\n---------------------------------\r\nData yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Sampai jumpa di Tim Sukses Loakin Go ;)', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_info_agen`
--

CREATE TABLE `ket_info_agen` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_info_agen`
--

INSERT INTO `ket_info_agen` (`id`, `keterangan`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Agen Silahkan Isi Data Di Bawah ini:\r\nNama:\r\nAlamat Rumah:\r\nRencana Alamat Agen (Jika sesuai dengan alamat rumah tidak perlu diisi):\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nApakah Alasan Anda ingin Buka Agen:\r\nApakah Anda Siap Menyiapkan Modal Investasi Bergabung? (Iya/ Tidak):\r\n---------------------------------\r\nData yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Sampai jumpa di Tim Sukses Loakin Go ;)', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_investasi`
--

CREATE TABLE `ket_investasi` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_investasi`
--

INSERT INTO `ket_investasi` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Outlet Silahkan Isi Data Di Bawah ini:\r\nNama:\r\nAlamat Rumah:\r\nRencana Alamat Outlet (Jika sesuai dengan alamat rumah tidak perlu diisi):\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nApakah Alasan Anda ingin Buka Outlet:\r\nApakah Anda Siap Menyiapkan Modal Investasi Bergabung? (Iya/ Tidak):\r\n---------------------------------\r\nData yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Sampai jumpa di Tim Sukses Loakin Go ;)', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_item`
--

CREATE TABLE `ket_item` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_item`
--

INSERT INTO `ket_item` (`id`, `keterangan`, `deleted`) VALUES
(1, 'Hallo Admin, Saya ingin Order Produk di bawah ini:\r\n\r\nNama Produk:\r\nJumlah produk:\r\nNama Penerima:\r\nAlamat pengiriman:\r\nNo Wa Penerima:\r\n\r\n----------------------\r\nNB:\r\n- Biaya ongkir ditanggung pembeli (Namun Kami akan berusaha untuk memberikan pilihan Kurir yg terbaik)\r\n- Setelah data Orderan masuk, maka kami akan memberikan Bill Total Pembayarannnya\r\n- Bank Available: BRI, BNI, BCA, dan BTN\r\n- Wajib mengirimkan foto bukti transfer\r\n- Barang akan kami proses setelah Konsumen mengirimkan foto bukti transfer\r\n-----------------------\r\n\r\nLoakin Go\r\n~Memudahkan & Mendekatkan Menjual Sampah untuk Didaur Ulang\r\n\r\n#SalamBantuBumi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_sponsor`
--

CREATE TABLE `ket_sponsor` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_sponsor`
--

INSERT INTO `ket_sponsor` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Event Sponshorship Silahkan Isi Data Di Bawah ini:\r\nNama Penyelenggara:\r\nNama Acara:\r\nDeskripsi singkat acara:\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nTanggal/ Jadwal Acara:\r\nAlamat Acara:\r\n---------------------------------\r\nData pengaju sponsor yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Terimakasih :)', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ket_waste`
--

CREATE TABLE `ket_waste` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_waste`
--

INSERT INTO `ket_waste` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Untuk Pendaftaran & Info Agen/ Outlet Center Silahkan Isi Data Di Bawah ini:\r\nNama Penyelenggara:\r\nNama Event:\r\nAlamat Event:\r\nNo Hp (WA Aktif):\r\nAlamat email:\r\nTanggal/ Jadwal Event:\r\nDeskripsi Singkat Event:\r\n--------------------------- \r\nData yang sudah terkirim akan Kami rekap terlebih dahulu. Anda nanti akan dihubungi oleh Costumer Care Loakin Go. Bersama Loakin Go yuk ciptakan Zero Waste on Event ;)', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ke_order_sampah`
--

CREATE TABLE `ke_order_sampah` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ke_order_sampah`
--

INSERT INTO `ke_order_sampah` (`id`, `keterangan`, `deleted`) VALUES
(1, 'Format Order Layanan Loakin Go :\r\nNama :\r\nAlamat pengambilan :\r\nHari/Tgl pengambilan :\r\nJam pengambilan :\r\nAlamat email :\r\nPerkiraan jumlah loakan kertas (Banyak/Sedikit) :', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontak_kami`
--

CREATE TABLE `kontak_kami` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak_kami`
--

INSERT INTO `kontak_kami` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Hallo Sobat Loakin Go, silahkan tuliskan pertanyaanmu di bawah ini:\r\nNama:\r\nAlamat:\r\nPertanyaan:\r\n------------------------\r\nData yang sudah dikirim akan masuk ke Costumer Care Loakin Go untuk direspon sesegera mungkin :)', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE `outlet` (
  `id` int(11) NOT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `file` text,
  `outlet` text,
  `nama` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`id`, `no_hp`, `file`, `outlet`, `nama`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '6285748233712', 'malang.jpg', 'Jl. tirto Wonokusumo', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(255) DEFAULT NULL,
  `kategori_produk` int(11) NOT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama_produk`, `kategori_produk`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Plastik A', 1, 'plan.png', NULL, NULL, '2019-06-07 02:38:27', NULL, 0),
(2, 'Komputer', 4, 'IMG-20190605-WA0009.jpg', '2019-06-07', NULL, '2019-06-07 02:37:38', NULL, 1),
(3, 'tes', 1, 'Screenshot_20190617-092559_TrayekAngkot.jpg', '2019-06-21', NULL, '2019-06-21 16:56:46', NULL, 0),
(4, 'gg', 1, 'IMG-20190622-WA0006.jpg', '2019-06-22', NULL, NULL, NULL, 0),
(5, 'u', 1, 'IMG-20190621-WA0021.jpg', '2019-06-22', NULL, '2019-06-22 04:55:09', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `produk_harga`
--

CREATE TABLE `produk_harga` (
  `id` int(11) NOT NULL,
  `produk` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `harga_antar` int(11) NOT NULL DEFAULT '0',
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_harga`
--

INSERT INTO `produk_harga` (`id`, `produk`, `harga`, `harga_antar`, `period_start`, `period_end`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 3000, 0, '2019-05-01', '2019-06-07', NULL, NULL, '2019-06-07 02:38:27', NULL, 0),
(2, 2, 30000, 35000, '2019-06-07', '2019-06-07', '2019-06-07', NULL, '2019-06-07 02:37:28', NULL, 0),
(3, 2, 30000, 35000, '2019-06-07', NULL, '2019-06-07', NULL, NULL, NULL, 0),
(4, 1, 3000, 3400, '2019-06-07', NULL, '2019-06-07', NULL, NULL, NULL, 0),
(5, 3, 3000, 5000, '2019-06-21', '2019-06-21', '2019-06-21', NULL, '2019-06-21 16:56:46', NULL, 0),
(6, 3, 3000, 5000, '2019-06-21', '2019-06-21', '2019-06-21', NULL, '2019-06-21 16:56:46', NULL, 0),
(7, 3, 3000, 5000, '2019-06-21', NULL, '2019-06-21', NULL, NULL, NULL, 0),
(8, 4, 3000, 5000, '2019-06-22', NULL, '2019-06-22', NULL, NULL, NULL, 0),
(9, 5, 6009, 8000, '2019-06-22', '2019-06-22', '2019-06-22', NULL, '2019-06-22 04:55:09', NULL, 0),
(10, 5, 6009, 8000, '2019-06-22', NULL, '2019-06-22', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `syarat_agen`
--

CREATE TABLE `syarat_agen` (
  `id` int(11) NOT NULL,
  `syarat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tes`
--

CREATE TABLE `tes` (
  `tes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tes`
--

INSERT INTO `tes` (`tes`) VALUES
('ika ingin bergabung menjadi agen sebuah bisnis tertentu, pilihlah bisnis yang marketable, sudah pasti dibutuhkan oleh Masyarakat, serta mampu bertahan jangka panjang. \"Bukan bisnis musiman atauhanya trend sesaat\".\r\n\r\nSadarkah Kami jika Masyarakat membeli ayam kentaki, kopi, makanan di restoran/warung, baju, dll di tempat/toko sama paling cepat hanya 1x/minggu. Tetapi Mereka menghasilkan sampah (bernilai ekonomis) hampir setiap hari, karena setiap Mereka berbelanja pasti akan membawa bungkus/pack yang terbuang sia-sia sebagai sampah karena tidak tahu harus menjualnya ke mana.');

-- --------------------------------------------------------

--
-- Table structure for table `total_agen`
--

CREATE TABLE `total_agen` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_agen`
--

INSERT INTO `total_agen` (`id`, `total`, `updateddate`, `updatedby`) VALUES
(1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `total_daerah`
--

CREATE TABLE `total_daerah` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_daerah`
--

INSERT INTO `total_daerah` (`id`, `total`, `updateddate`, `updatedby`) VALUES
(1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `total_mitra`
--

CREATE TABLE `total_mitra` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_mitra`
--

INSERT INTO `total_mitra` (`id`, `total`, `updateddate`, `updatedby`) VALUES
(1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `total_outlet`
--

CREATE TABLE `total_outlet` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_outlet`
--

INSERT INTO `total_outlet` (`id`, `total`, `updateddate`, `updatedby`) VALUES
(1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `total_sampah`
--

CREATE TABLE `total_sampah` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_sampah`
--

INSERT INTO `total_sampah` (`id`, `total`, `updateddate`, `updatedby`) VALUES
(1, 45, '2019-07-20 17:57:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `total_user`
--

CREATE TABLE `total_user` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_user`
--

INSERT INTO `total_user` (`id`, `total`, `updateddate`, `updatedby`) VALUES
(1, 12, '2019-07-20 17:57:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `login_by` varchar(45) DEFAULT NULL COMMENT 'gmail\nfb\nreguler',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `is_login` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `no_hp`, `login_by`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`, `is_login`) VALUES
(1, NULL, 'dodikitn@gmail.com', NULL, NULL, 'gmail', '2019-05-28', NULL, NULL, NULL, 0, 0),
(2, NULL, 'Dodik Rismawan Affrudin', NULL, NULL, 'fb', '2019-05-28', NULL, NULL, NULL, 0, 0),
(4, 'bejos', 'bejo@gmail.com', '1234', '08754333444', 'reguler', '2019-05-28', NULL, '2019-06-20 05:36:29', NULL, 0, 0),
(5, NULL, 'solutionsdapps@gmail.com', NULL, NULL, 'gmail', '2019-07-01', NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin`
--

CREATE TABLE `user_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_admin`
--

INSERT INTO `user_admin` (`id`, `username`, `password`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'admin', 'admin', NULL, NULL, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agen`
--
ALTER TABLE `agen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area_service`
--
ALTER TABLE `area_service`
  ADD PRIMARY KEY (`id`,`area`),
  ADD KEY `fk_area_service_area_idx` (`area`);

--
-- Indexes for table `info_admin`
--
ALTER TABLE `info_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_agen`
--
ALTER TABLE `ket_agen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_area`
--
ALTER TABLE `ket_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_donasi_pohon`
--
ALTER TABLE `ket_donasi_pohon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_donasi_sampah`
--
ALTER TABLE `ket_donasi_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_harga`
--
ALTER TABLE `ket_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_info_agen`
--
ALTER TABLE `ket_info_agen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_investasi`
--
ALTER TABLE `ket_investasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_item`
--
ALTER TABLE `ket_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_sponsor`
--
ALTER TABLE `ket_sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ket_waste`
--
ALTER TABLE `ket_waste`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ke_order_sampah`
--
ALTER TABLE `ke_order_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak_kami`
--
ALTER TABLE `kontak_kami`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`,`kategori_produk`),
  ADD KEY `fk_produk_kategori_produk1_idx` (`kategori_produk`);

--
-- Indexes for table `produk_harga`
--
ALTER TABLE `produk_harga`
  ADD PRIMARY KEY (`id`,`produk`),
  ADD KEY `fk_produk_harga_produk1_idx` (`produk`);

--
-- Indexes for table `syarat_agen`
--
ALTER TABLE `syarat_agen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_agen`
--
ALTER TABLE `total_agen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_daerah`
--
ALTER TABLE `total_daerah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_mitra`
--
ALTER TABLE `total_mitra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_outlet`
--
ALTER TABLE `total_outlet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_sampah`
--
ALTER TABLE `total_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_user`
--
ALTER TABLE `total_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admin`
--
ALTER TABLE `user_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agen`
--
ALTER TABLE `agen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `area_service`
--
ALTER TABLE `area_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `info_admin`
--
ALTER TABLE `info_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ket_agen`
--
ALTER TABLE `ket_agen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_area`
--
ALTER TABLE `ket_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_donasi_pohon`
--
ALTER TABLE `ket_donasi_pohon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_donasi_sampah`
--
ALTER TABLE `ket_donasi_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_harga`
--
ALTER TABLE `ket_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_info_agen`
--
ALTER TABLE `ket_info_agen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_investasi`
--
ALTER TABLE `ket_investasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_item`
--
ALTER TABLE `ket_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_sponsor`
--
ALTER TABLE `ket_sponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ket_waste`
--
ALTER TABLE `ket_waste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ke_order_sampah`
--
ALTER TABLE `ke_order_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kontak_kami`
--
ALTER TABLE `kontak_kami`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `produk_harga`
--
ALTER TABLE `produk_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `syarat_agen`
--
ALTER TABLE `syarat_agen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `total_agen`
--
ALTER TABLE `total_agen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `total_daerah`
--
ALTER TABLE `total_daerah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `total_mitra`
--
ALTER TABLE `total_mitra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `total_outlet`
--
ALTER TABLE `total_outlet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `total_sampah`
--
ALTER TABLE `total_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `total_user`
--
ALTER TABLE `total_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_admin`
--
ALTER TABLE `user_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
